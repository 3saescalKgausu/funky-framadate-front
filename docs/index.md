# Framadate - funky version

FR: Un logiciel libre de sondage fait par les contributeurs de l'association Framasoft, avec une API backend.
***
EN: A libre polling software made by contributors around the French association Framasoft.
This version uses a brand new backend API.ntributeurs de l'association Framasoft, avec une API backend.

## Index

* Meeting notes
* Getting Started (yarn start / npm start)
* How to contribute
* Architecture
* Translation i18n
* Accesibility
* Licence GNU affero V3

# Framadate - funky version
FR: Un logiciel libre de sondage fait par les contributeurs de l'association Framasoft, avec une API backend.
EN: A libre polling software made by contributors around the French association Framasoft.
This version uses a brand new backend API.

## Pour débuter - getting started
[lire la doc pour débuter votre Funky Framadate](GETTING_STARTED.md)

## Documentation
voilà voilà

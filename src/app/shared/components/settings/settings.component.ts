import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Owner } from '../../../core/models/owner.model';
import { UserService } from '../../../core/services/user.service';

@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
	public user: Owner;
	private userSubscription: Subscription;

	constructor(private userService: UserService, public dialogRef: MatDialogRef<SettingsComponent>) {}

	ngOnInit(): void {}

	ngOnDestroy(): void {
		if (this.userSubscription) {
			this.userSubscription.unsubscribe();
		}
	}

	public saveChanges(): void {
		if (this.user?.pseudo?.length) {
			this.userService.updateUser(this.user);
		}
		this.closeDialog();
	}

	public closeDialog(): void {
		this.dialogRef.close();
	}
}

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Poll } from '../models/poll.model';
import { ApiService } from './api.service';
import { UserService } from './user.service';
import { UuidService } from './uuid.service';

@Injectable({
	providedIn: 'root',
})
export class MockingService {
	private _pollsAvailables: BehaviorSubject<Poll[]> = new BehaviorSubject<Poll[]>([]);
	public readonly pollsAvailables: Observable<Poll[]> = this._pollsAvailables.asObservable();

	constructor(private apiService: ApiService, private userService: UserService, private uuidService: UuidService) {}

	public async init(): Promise<void> {}

	public loadMock(): void {}
}

import { Choice, ChoiceGroup } from './choice.model';
import { DateChoice, TimeSlices } from './dateChoice.model';

export class PollDTO {
	adminKey; // key to change config of the poll
	allowSeveralHours;
	answers;
	canModifyAnswers; // bool for the frontend selector
	created_at;
	currentPoll; // current poll selected with createPoll or getPoll of ConfigService
	custom_url; // custom slug in the url; must be unique
	customUrlIsUnique; // given by the backend
	dateList; // sets of days as strings; config to set identical time for days in a special days poll
	deletionDateAfterLastModification;
	description;
	displayConfirmVoteModalAdmin;
	default_expiracy_days_from_now;
	expirationDate; // expiracy date
	isAdmin; // when we create a poll; we are admin on it
	kind: string;
	myComment = '';
	myEmail;
	myName;
	myPolls; // list of retrieved polls from the backend api
	myTempVoteStack;
	myVoteStack;
	owner_modifier_token; // key to change a vote stack
	password;
	passwordAccess;
	pollId; // id of the current poll when created. data given by the backend api
	pollSlug; // id of the current poll when created. data given by the backend api
	pollType: string = 'date'; // classic or dates
	timeList; // ranges of time expressed as strings
	title;
	urlAdmin;
	urlPublic;
	urlSlugPublic;
	visibility; // visible to one with the link:
	voteChoices = 'yes; maybe; no'; // possible answers to a vote timeSlice: only "yes"; "yes; maybe; no"
	voteStackId; // id of the vote stack to update
	whoCanChangeAnswers; // everybody; self; nobody (: just admin)
	whoModifiesAnswers; // everybody; self; nobody (: just admin)

	constructor() {}
}

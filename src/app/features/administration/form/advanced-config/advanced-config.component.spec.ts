import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedConfigComponent } from './advanced-config.component';

describe('AdvancedConfigComponent', () => {
	let component: AdvancedConfigComponent;
	let fixture: ComponentFixture<AdvancedConfigComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [AdvancedConfigComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AdvancedConfigComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
